# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### 0.1.1 (2020-09-26)


### Features

* **add:** ✨ ➕ Add files ([c2a6c75](https://gitlab.com/tomgrv/project-template-npm/commit/c2a6c757dfc401dce7b689ea9031d3f853ea63ff))


### Bug Fixes

* 🐛 Correct hotfix handling ([207013d](https://gitlab.com/tomgrv/project-template-npm/commit/207013da1b741863b54fc93a584440267eb67388))

## 0.1.0 (2020-09-26)


### Features

* **add:** ✨ ➕ Add files ([c2a6c75](https://gitlab.com/tomgrv/project-template-npm/commit/c2a6c757dfc401dce7b689ea9031d3f853ea63ff))

# 0.0.0 (2020-09-25)
